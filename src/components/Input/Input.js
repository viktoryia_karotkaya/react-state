import "./Input.css";

const Input = (props) => {
  return (
    <input
      className={props.disabled ? `${props.class} button-disabled`: props.class }
      type={props.type}
      name={props.name}
      placeholder={props.placeholder}
      value={props.value}
      onChange={props.changeMethod}
    />
  );
}

export default Input;
