import "./Card.css";

const Card = ({user}) => {
  return (
    <div className="card">
      <img src={user.avatar} alt="avatar" width="150" height="150" />
      <div className="card-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
        incididunt ut labore et dolor.</div>
      <div className="card-fullname">{user.firstName} {user.lastName}</div>
      <div className="card-position">{user.position}</div>
    </div>
  )
}

export default Card;
