import {useReducer, useState} from "react";
import axios from '../../api/axios';
import Input from "../Input/Input";
import "./Form.css";


const Form = () => {
  const [isSubscribed, setIsSubscribed] = useState(false);
  const [email, setEmail] = useState('');
  const [sending, toggle] = useReducer(
    (sending) => !sending,
    false
  );
  
  const changeHandler = (e) => {
    setEmail(e.target.value);
  }
  
  const unsubscribeHandler = async(e) => {
    e.preventDefault();
    toggle();
    const user = {
      email: email
    };
    try {
      await axios.post('unsubscribe', user);
      setIsSubscribed(!isSubscribed);
      setEmail('');
    } catch (err) {
      if(err.response){
        alert(err.response.data.error);
      } else {
        console.log(`Error: ${err.message}`);
      }
    }
    toggle();
  }
  
  const subscribeHandler = async (e) => {
    e.preventDefault();
    toggle();
    const user = {
      email: email
    };
    try {
      await axios.post('subscribe', user);
      setIsSubscribed(!isSubscribed);
    } catch (err) {
      if(err.response){
        alert(err.response.data.error);
      } else {
        console.log(`Error: ${err.message}`);
      }
    }
    toggle();
  }
  
  return (
    <>
      { isSubscribed?
          <form onSubmit={unsubscribeHandler}>
          <Input
            class="button-subscribe"
            type="submit"
            value='Unsubscribe'
            disabled={sending}
          />
        </form>
            :
          <form onSubmit={subscribeHandler}>
          <Input
            class="input"
            type="email"
            placeholder="Email"
            value={email}
            name="email"
            changeMethod={changeHandler}
          /><Input
            class="button-subscribe"
            type="submit"
            value='Subscribe'
            disabled={sending}
          />
        </form>}
    </>
  );
}

export default Form;
