import {useState} from "react";
import Cards from "../Cards/Cards";
import Button from "../Button/Button";
import "./Community.css";

const Community = (props) => {
  const [showed, setShowed] = useState(props.showState);
  
  const toggle = () => {
    setShowed(!showed);
  }
  return (
    <section className="app-section section-community">
      <h2 className="app-title">
        Big community of<br />
        people like you
      </h2>
      <Button class="toggle-button" show={showed} toggleHandler={toggle}  />
      {showed && <div >
        <h3 className="app-subtitle">
          We’re proud of our products, and we’re really excited<br />
          when we get feedback from our users.
        </h3>
        {props.userList.length > 0 ?<Cards userList={props.userList} /> : null}
      </div>}
    </section>
  )
}

export default Community;
