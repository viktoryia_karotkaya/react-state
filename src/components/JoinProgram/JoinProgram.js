import "./JoinProgram.css";
import Form from "../Form/Form";

const JoinProgram = () => {
  return (
    <section className="app-section section-join-program">
      <h2 className="app-title">Join Our Program</h2>
      <h3 className="app-subtitle">
        Sed do eiusmod tempor incididunt<br />
        ut labore et dolore magna aliqua.
      </h3>
      <Form />
    </section>
  );
}

export default JoinProgram;
