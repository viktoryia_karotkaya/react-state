import "./Button.css";

const Button = (props) => {
  return (
    <button className={props.class} onClick={props.toggleHandler}>{props.show ? 'Hide section' : 'Show section'}</button>
  );
}

export default Button;
