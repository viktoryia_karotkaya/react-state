import Card from "../Card/Card";
import "./Cards.css";

const Cards = (props) => {
  return (
    <div className="community-cards">
      {props.userList.map((user, index) =>
        <Card user={user}  key={index} />
      )}
    </div>
  );
}

export default Cards;
