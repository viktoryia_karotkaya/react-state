import {useState, useEffect} from "react";
import axios from './api/axios';
import './App.css';
import JoinProgram from "./components/JoinProgram/JoinProgram";
import Community from "./components/Community/Community";

function App() {
  const [users, setUsers] = useState([]);
  const communitySection = (users.length > 0 ? <Community userList={users} showState={true} /> : <p>Loading...</p>);
  useEffect(() => {
    const fetchUsers = async() => {
      try {
        const res = await axios.get('community');
        setUsers(res.data);
      } catch (err) {
        if(err.response){
          console.log(err.response.data);
          console.log(err.response.status);
          console.log(err.response.headers);
        } else {
          console.log(`Error: ${err.message}`);
        }
      }
    }
    fetchUsers();
  }, []);

  return (
    <main className="App">
      {communitySection}
      <JoinProgram />
    </main>
  );
}

export default App;
